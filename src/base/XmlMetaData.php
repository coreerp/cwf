<?php
/**
 * @link http://www.vishwayon.com/
 * @copyright Copyright (c) 2008 Vishwayon Software Pvt Ltd
 * @license http://www.vishwayon.com/license/
 */

namespace cwf\base;

/**
 * XmlMetaData contains the xml data populated as
 * class properties
 *
 * @author Girish Shenoy <girish@vishwayon.com>
 */
class XmlMetaData implements ICwfType {
    
    /**
     * Contains the cwfType
     * @var string 
     */
    private $_cwfType = self::UNKNOWN;
    
    /**
     * Contains the metadata information
     * @var GenericCwfType 
     */
    public $metaData;
    
    public function getCwfType(): string {
        return $this->_cwfType;
    }
    
    public function setCwfType(string $cwfType) {
        $this->_cwfType = $cwfType;
    }
}
