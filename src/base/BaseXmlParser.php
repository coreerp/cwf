<?php

/**
 * @link http://www.vishwayon.com/
 * @copyright Copyright (c) 2008 Vishwayon Software Pvt Ltd
 * @license http://www.vishwayon.com/license/
 */

namespace cwf\base;

/**
 * BaseXmlParser is the base class for reading all xml formatted
 * Business Objects, Forms, Collections, etc. that comply with
 * coreWebFramework (CwFramework.xsd) schema definition
 *
 * @author Girish Shenoy <girish@vishwayon.com>
 */
class BaseXmlParser {

    /**
     * Parses the cwFramework.xsd compliant xml file and returns 
     * an instance of XmlMetaData
     */
    public static function parseFile(string $xmlFile): XmlMetaData {
        if (!file_exists($xmlFile)) {
            throw new \Exception("File not Found: \n$xmlFile");
        }
        $cwfXml = simplexml_load_file($xmlFile);
        $xmlMetaData = new XmlMetaData();
        self::parseRoot($cwfXml, $xmlMetaData);
        return $xmlMetaData;
    }

    /**
     * This method parses the rootElement of CwFramework and 
     * sets the ICwfType
     * 
     * @param \SimpleXMLElement $cwfXml The Root Xml element
     * @param \cwf\base\XmlMetaData $xmlMetaData An instance of the resulting class
     */
    protected static function parseRoot(\SimpleXMLElement $cwfXml, XmlMetaData $xmlMetaData) {
        if (isset($cwfXml->{ICwfType::BUSINESS_OBJECT})) {
            $xmlMetaData->setCwfType(ICwfType::BUSINESS_OBJECT);
            self::parseBusinessObject($cwfXml->{ICwfType::BUSINESS_OBJECT}, $xmlMetaData);
        } elseif (isset($cwfXml->{ICwfType::FORM_VIEW})) {
            $xmlMetaData->setCwfType(ICwfType::FORM_VIEW);
            $xmlMetaData->metaData = self::parseFormView($cwfXml->formView, $xmlMetaData);
        } elseif (isset($cwfXml->{ICwfType::COLLECTION_VIEW})) {
            $xmlMetaData->setCwfType(ICwfType::COLLECTION_VIEW);
            self::parseCollectionView($cwfXml->collectionView, $xmlMetaData);
        } elseif (isset($cwfXml->{ICwfType::ALLOC_VIEW})) {
            $xmlMetaData->setCwfType(ICwfType::ALLOC_VIEW);
            self::parseAllocView($cwfXml->allocView, $xmlMetaData);
        } elseif (isset($cwfXml->{ICwfType::REPORT_VIEW})) {
            $xmlMetaData->setCwfType(ICwfType::REPORT_VIEW);
            self::parseReportView($cwfXml->reportView, $xmlMetaData);
        }
    }

    protected static function parseFormView(\SimpleXMLElement $xFormView): CwfGenericType {
        $fv = new CwfGenericType('formView');
        $fv->set('header', (string) $xFormView->header ?? '');
        $fv->set('keyField', (string) $xFormView->keyField ?? '');
        $fv->set('printView', function() use($xFormView) {
            if (isset($xFormView->printView)) {
                $rptParams = [];
                if (isset($xFormView->printView->rptParams)) {
                    foreach($xFormView->printView->rptParams->param as $xparam) {
                        $rptParams[(string)$xparam->attributes()['id']] = (string) $xparam;
                    }
                }
                return $rptParams;
            }
        });
        $fv->set('summaryView', (string) $xFormView->summaryView ?? '');
        $fv->set('newDocEnabled', function() use($xFormView) {
            if (isset($xFormView->newDocEnabled)) {
                $xnde = $xFormView->newDocEnabled;
                $nde = new \cwf\base\CwfGenericType('newDocEnabled');
                $nde->set('docType', (string) $xnde->docType ?? '');
                $nde->set('beforeNewEvent', (string) $xnde->beforeNewEvent ?? '');
                $nde->set('afterNewEvent', (string) $xnde->afterNewEvent ?? '');
                $nde->set('wizard', (string) $xnde->wizard ?? '');
                $nde->set('step', (string) $xnde->step ?? '');
                $nde->set('label', (string) $xnde->label ?? '');
                return $nde;
            } else {
                return null;
            }
        });
        $fv->set('deleteDocEnabled', (bool) $xFormView->deleteDocEnabled ?? false);
        $fv->set('unpostDisabled', (bool) $xFormView->unpostDisabled ?? true);
        $fv->set('clientJsCode', function() use ($xFormView) {
            $jsFiles = [];
            if (isset($xFormView->clientJsCode)) {
                $jsFiles[] = (string) $xFormView->clientJsCode;
            }
            if (isset($xFormView->clientJsCodeRefs)) {
                $xcjcrs = $xFormView->clientJsCodeRefs;
                foreach($xcjcrs->clientJsCodeRef as $xcjr) {
                    $jsFiles[] = (string) $xcjr;
                }
            }
            return $jsFiles;
        });
        $fv->set('jsEvents', function() use ($xFormView) { 
            $jsEvents = [];
            if (isset($xFormView->jsEvents)) {
                if (isset($xFormView->jsEvents->afterLoadEvent)) {
                    $jsEvents[] = (string) $xFormView->jsEvents->afterLoadEvent;
                }
                if (isset($xFormView->jsEvents->afterUnpostEvent)) {
                    $jsEvents[] = (string) $xFormView->jsEvents->afterUnpostEvent;
                }
                if (isset($xFormView->jsEvents->afterPostEvent)) {
                    $jsEvents[] = (string) $xFormView->jsEvents->afterPostEvent;
                }
                if (isset($xFormView->jsEvents->afterRefreshEvent)) {
                    $jsEvents[] = (string) $xFormView->jsEvents->afterRefreshEvent;
                }
            }
            return $jsEvents;
        });
        $fv->set('codeBehind', function() use($xFormView) {
            if (isset($xFormView->codeBehind)) {
                if (isset($xFormView->codeBehind->className)) {
                    return (string) $xFormView->codeBehind->className;
                }
            }
            return '';
        });
        isset($xFormView->controlSection) ? $fv->set('controlSection', self::parseFromDatabinding($xFormView->controlSection)) : '';
        $fv->set('dmFiles', function() use($xFormView) {
            if(isset($xFormView->dmFiles)) {
                return true;
            }
            return false;
        });
        
        // Load Form Attributes
        $fv->setAttr('id', (string) $xFormView->attributes()['id'] ?? '');
        $fv->setAttr('type', (string) $xFormView->attributes()['type'] ?? '');
        $fv->setAttr('bindingBO', (string) $xFormView->attributes()['bindingBO'] ?? '');
        $fv->setAttr('helpLink', (string) $xFormView->attributes()['helpLink'] ?? '');
        return $fv;
    }
    
    public static function parseFromDatabinding(\SimpleXMLElement $xFDataBind): CwfGenericType {
        $cs = new CwfGenericType('controlSection');
        $cs->setAttr('editMode', (string) $xFDataBind->attributes()['editMode'] ?? 'Add|Edit|Delete');
        $cs->setAttr('dataProperty', (string) $xFDataBind->attributes()['dataProperty'] ?? '');
        $cs->setAttr('bindMethod', (string) $xFDataBind->attributes()['bindMethod'] ?? '');
        $cs->setAttr('crudOn', (string) $xFDataBind->attributes()['crudOn'] ?? '');
        $cs->setAttr('addFirst', (string) $xFDataBind->attributes()['addFirst'] ?? '');
        
        $fdb = new CwfGenericType('dataBinding');
        foreach($xFDataBind->dataBinding->children() as $nodeName => $nodeDef) {
            switch ($nodeName) {
                case 'field':
                    $fdb->set('', function() use($nodeDef) {
                        $field = new CwfGenericType('field');
                        $field->set('id', (string) $nodeDef->attributes()['id'] ?? '');
                        $field->set('label', (string) $nodeDef->attributes()['label'] ?? '');
                        $field->set('dataType', (string) $nodeDef->attributes()['type'] ?? '');
                        $field->set('control', (string) $nodeDef->attributes()['control'] ?? '');
                        
                        return $field;
                    });
            }
        }
        $cs->set('dataBinding', $fdb);
        return $cs;
    }

}
