<?php

/**
 * @link http://www.vishwayon.com/
 * @copyright Copyright (c) 2008 Vishwayon Software Pvt Ltd
 * @license http://www.vishwayon.com/license/
 */

namespace cwf\base;

/**
 * ICwfType interface defines the basic attributes 
 * implemented by the cwFramework schema
 *
 * @author Girish Shenoy <girish@vishwayon.com>
 */
interface ICwfType {
    
    const UNKNOWN = 'unknown';
    const BUSINESS_OBJECT = 'businessObject';
    const COLLECTION_VIEW = 'collectionView';
    const LOOKUP = 'lookup';
    const REPORT_VIEW = 'reportView';
    const FORM_VIEW = 'formView';
    const ALLOC_VIEW = 'allocView';

    /**
     * Returns one of the CwfConstants
     * self::BUSINESS_OBJECT
     * self::COLLECTION_VIEW
     * self::LOOKUP
     * self::REPORT_VIEW
     * self::FORM_VIEW
     * self::ALLOC_VIEW
     */
    public function getCwfType(): string;
}
