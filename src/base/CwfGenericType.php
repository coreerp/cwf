<?php

/**
 * @link http://www.vishwayon.com/
 * @copyright Copyright (c) 2008 Vishwayon Software Pvt Ltd
 * @license http://www.vishwayon.com/license/
 */

namespace cwf\base;

/**
 * GenericCwfType represents any/all types
 * implemented by the cwFramework schema
 *
 * @author Girish Shenoy <girish@vishwayon.com>
 */
class CwfGenericType {

    /**
     * Contains a collection of child elements
     * @var array() 
     */
    private $_elements = [];
    private $_attributes = [];
    private $_cwfType = '';
    
    public function __construct(string $cwfType) {
        $this->_cwfType = $cwfType;
    }

    /**
     * Sets/adds an element to the self collection
     * @param string $elName Name of the element to set
     * @param type $elValue  Value of the element to set
     */
    public function set(string $elName, $elValue) {
        if ($elName != '') {
            if ($elValue instanceof \Closure) {
                $this->_elements[$elName] = $elValue();
            } else {
                $this->_elements[$elName] = $elValue;
            }
        } else {
            if ($elValue instanceof \Closure) {
                $this->_elements[] = $elValue();
            } else {
                $this->_elements[] = $elValue;
            }
        }
    }

    /**
     * Sets/adds an element to attribute to the collection
     * @param string $attrName Name of the attribute to set
     * @param type $attrValue  Value of the attribute to set
     */
    public function setAttr(string $attrName, $attrValue) {
        $this->_attributes[$attrName] = $attrValue;
    }

    /**
     * Gets the element value or the default (if not available)
     * This is a safe method and does not raise an exception 
     * when the element is missing
     * 
     * @param string $elName    Name of the element to find
     * @param mixed $elDefault  Pass default value when element is not found
     * @return mixed            Returns element value. If not found, returns $elDefault
     */
    public function get(string $elName, $elDefault) {
        if (array_key_exists($elName, $this->_elements)) {
            return $this->_elements[$elName];
        }
        return $elDefault;
    }

    /**
     * Checks if the element is available in the collection
     * 
     * @param string $elName    Name of the element
     * @return bool             Returns true if element found in collection
     */
    public function has(string $elName): bool {
        return array_key_exists($elName, $this->_elements);
    }
    
    /**
     * Gets the CwfType for current instance
     * @return string 
     */
    public function getType(): string {
        return $this->_cwfType;
    }

}
